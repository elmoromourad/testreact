import { transformData } from "../utils/utils";

const requestData = (requestData) => {
  return fetch('https://api.nasa.gov/neo/rest/v1/neo/browse?api_key=DEMO_KEY')
  .then(res => res.json())
    .then(
      async (result) => {
        return await transformData(result);
      },
      (error) => {
        return error;
      }
    );
};

export default requestData;
