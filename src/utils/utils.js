export const transformData = async (nasaData) => {
  const labels = [];
  const minEstimated = [];
  const maxEstimated = [];
  const rows = [];

  nasaData.near_earth_objects.map((element, index) => {
    labels.push(element.name);
    minEstimated.push(
      element.estimated_diameter.kilometers.estimated_diameter_min
    );
    maxEstimated.push(
      element.estimated_diameter.kilometers.estimated_diameter_max
    );
    rows.push({
      id: index + 1,
      neoName: element.name,
      minEstimated:
        element.estimated_diameter.kilometers.estimated_diameter_min,
      maxEstimated:
        element.estimated_diameter.kilometers.estimated_diameter_max,
    });
    return null;
  });

  return {
    labels,
    minEstimated,
    maxEstimated,
    rows,
  };
};
