import * as React from "react";
import { DataGrid } from "@material-ui/data-grid";

const Grid = (props) => {
  const { rows, columns } = props;
  return <DataGrid rows={rows} columns={columns} />;
};

export default Grid;
