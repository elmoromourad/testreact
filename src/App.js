import { Component } from "react";
import * as React from "react";
import { Button } from "@material-ui/core";
import "./App.css";
import Grid from "./Components/Grid";
import Chart from "./Components/Chart";
import requestData from "./api/service";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedTab: "",
      data: {
        labels: ["Red", "Blue"],
        datasets: [],
      },
      options: {
        indexAxis: "y",
        elements: {
          bar: {
            borderWidth: 2,
          },
        },
        responsive: true,
        plugins: {
          legend: {
            position: "top",
          },
          title: {
            display: true,
            text: "Max Estimated Diameter (km)",
          },
        },
      },
      columns: [
        { field: "id", headerName: "ID", width: 70 },
        { field: "neoName", headerName: "Neo name", width: 130 },
        {
          field: "minEstimated",
          headerName: "Min Estimated Diameter (km)",
          width: 130,
        },
        {
          field: "maxEstimated",
          headerName: "Max Estimated Diameter (km)",
          width: 130,
        },
      ],
      rows: [],
    };
  }

  async componentDidMount() {
    const { datasets } = this.state.data;
    const response = await requestData();

    datasets[0] = {
      data: response.minEstimated,
      label: "Min Estimated Diameter (km)",
      backgroundColor: ["blue"],
    };
    datasets[1] = {
      data: response.maxEstimated,
      label: "Max Estimated Diameter (km)",
      backgroundColor: ["red"],
    };
    this.setState({
      selectedTab: "A",
      rows: response.rows,
      data: {
        labels: response.labels,
        datasets,
      },
    });
  }

  render() {
    const { selectedTab } = this.state;
    return (
      <div className="App">
        <header className="App-header">
          <div style={{ height: 600, width: 800 }}>
            <Button onClick={() => this.setState({ selectedTab: "A" })}>Chart</Button>
            <Button onClick={() => this.setState({ selectedTab: "B" })}>Grid</Button>
            {selectedTab === "A" && (
              <Chart data={this.state.data} options={this.state.options} />
            )}
            {selectedTab === "B" && (
              <Grid rows={this.state.rows} columns={this.state.columns} />
            )}
          </div>
        </header>
      </div>
    );
  }
}

export default App;
